Name: libnftnl
Version: 1.2.8
Release: 1
Summary: Library for low-level interaction with nftables Netlink's API over libmnl
License: GPL-2.0-or-later
URL: https://www.netfilter.org/projects/libnftnl/index.html
Source0: https://www.netfilter.org/projects/libnftnl/files/%{name}-%{version}.tar.xz

BuildRequires: pkgconfig(libmnl) >= 1.0.4
BuildRequires: gcc

# replace old libnftables package
Provides: libnftables = %{version}-%{release}
Obsoletes: libnftables < 0-0.6

%description
libnftnl is a userspace library providing a low-level netlink programming interface (API)
to the in-kernel nf_tables subsystem.

%package devel
Summary: Development files for %{name}
Requires: %{name} = %{version}-%{release}
# replace old libnftables-devel package
Provides: libnftables-devel = %{version}-%{release}
Obsoletes: libnftables-devel < 0-0.6

%description devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%prep
%autosetup -n %{name}-%{version} -p1

%build
%configure --disable-static --disable-silent-rules
%make_build

%install
%make_install
%delete_la

%check
%make_build check

%files
%license COPYING
%{_libdir}/*.so.*

%files devel
%{_libdir}/libnft*.so
%{_libdir}/pkgconfig/libnftnl.pc
%{_includedir}/libnftnl

%changelog
* Thu Oct 03 2024 Funda Wang <fundawang@yeah.net> - 1.2.8-1
- update to 1.2.8

* Wed Jul 17 2024 Funda Wang <fundawang@yeah.net> - 1.2.7-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:update version to 1.2.7

* Tue Sep 05 2023 shixuantong <shixuantong1@huawei.com> - 1.2.6-2 
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:set: Do not leave free'd expr_list elements in place

* Mon Jul 17 2023 zhangchenglin <zhangchenglin@kylinos.cn> - 1.2.6-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:update version to 1.2.6

* Sat Apr 01 2023 shixuantong <shixuantong1@huawei.com> - 1.2.5-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:update version to 1.2.5

* Thu Nov 17 2022 fuanan <fuanan3@h-partners.com> - 1.2.4-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:update version to 1.2.4

* Tue Oct 25 2022 yanglongkang <yanglongkang@h-partners.com> - 1.2.1-2
- rebuild for next release

* Tue Mar 29 2022 panxiaohe <panxh.life@foxmail.com> - 1.2.1-1
- update to 1.2.1

* Mon Jul 19 2021 fuanan <fuanan3@huawei.com> - 1.2.0-1
- update to 1.2.0

* Thu Jan 21 2021 yangzhuangzhuang<yangzhuangzhuang1@huawei.com> - 1.1.8-1
- update to 1.1.8

* Wed Sep 9 2020 wangchen<wangchen137@huawei.com> - 1.1.7-2
- modify the URL of Source0

* Wed Jul 22 2020 hanzhijun<hanzhijun1@huawei.com> - 1.1.7-1
- update to 1.1.7

* Wed Jul 1 2020 chengquan<chengquan3@huawei.com> - 1.1.5-3
- delete temporary binary compatibility

* Tue Apr 28 2020 Yufa Fang<fangyufa1@huawei.com> - 1.1.5-2
- include previous ABI version for temporary binary compatibility

* Fri Apr 24 2020 Yufa Fang<fangyufa1@huawei.com> - 1.1.5-1
- update to 1.1.5 and fix flowtable tests failure

* Tue Sep 17 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.1.1-6
- Package init

